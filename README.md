# HashCash

Website where you buy stocks in hashtags. Value of the stocks is the hourly tweet volume / 100.

#### Production

Develop branch is running at http://hashex.ca

TODO: Pipeline for master to push here automatically!

#### Long Term

Road map includes things like
- Lottery: Take a chance at winning (static price or maybe dynamic % of all trades last 24 hrs?) dollars.
- Boosts: Something else to buy like an additional research point or the one off ability to invest in a stock at past value. Need to figure out how that should impact networth